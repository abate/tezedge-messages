// Copyright (c) SimpleStaking, Viable Systems and Tezedge Contributors
// SPDX-License-Identifier: MIT

//! Operation contents. This is the contents of the opaque field [super::operation::Operation::data].
//!
//! Changes comparing to the [`proto_014::operation::Operation`](crate::proto_014::operation::Operation):
//!
//! TODO
pub use super::super::proto_014::operation::{
    ActivateAccountOperation, BallotOperation, ContractId, DelegationOperation,
    DoubleBakingEvidenceOperation, DoubleEndorsementEvidenceOperation,
    DoublePreendorsementEvidenceOperation, EndorsementOperation, FailingNoopOperation,
    OriginationOperation, PreendorsementOperation, ProposalsOperation, RevealOperation,
    SeedNonceRevelationOperation, SetDepositsLimitOperation,
};

use crypto::{
    base58::ToBase58Check,
    hash::{BlockHash, BlockPayloadHash, ChainId, HashTrait, Signature},
    CryptoError,
};
use tezos_encoding::{
    binary_reader::BinaryReaderError, binary_writer::BinaryWriterError, enc::put_bytes,
    enc::BinWriter, encoding::HasEncoding, nom::NomReader, types::Mutez,
};

use crate::{
    base::signature_public_key::{SignaturePublicKey, SignaturePublicKeyHash, SignatureWatermark},
    p2p::{binary_message::BinaryWrite, encoding::operation::Operation as P2POperation},
    protocol::{proto_011::operation::RegisterGlobalConstantOperation, FromShell},
};

pub use super::super::proto_008_2::operation::ShortDynamicData;

/// alpha.entrypoint.
/// See https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#alpha-entrypoint-determined-from-data-8-bit-tag.
#[cfg_attr(feature = "fuzzing", derive(fuzzcheck::DefaultMutator))]
#[derive(Debug, Clone, HasEncoding, NomReader)]
pub enum Entrypoint {
    /// default (tag 0).
    /// See https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#default-tag-0.
    Default,

    /// root (tag 1).
    /// See https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#root-tag-1.
    Root,

    /// do (tag 2).
    /// See https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#do-tag-2.
    Do,

    /// set_delegate (tag 3).
    /// See https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#set-delegate-tag-3.
    SetDelegate,

    /// remove_delegate (tag 4).
    /// See https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#remove-delegate-tag-4.
    RemoveDelegate,

    /// deposit (tag 5).
    /// See https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#remove-delegate-tag-5.
    Deposit,

    /// stake (tag 6).
    /// See https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#remove-delegate-tag-6.
    Stake,

    /// unstake (tag 7).
    /// See https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#remove-delegate-tag-7.
    Unstake,

    /// finalize_unstake (tag 8).
    /// See https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#remove-delegate-tag-8.
    FinalizeUnstake,

    /// set_delegate_parameters (tag 9).
    /// See https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#remove-delegate-tag-9.
    SetDelegateParameters,

    /// named (tag 255).
    /// See https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#named-tag-255.
    #[encoding(tag = 255)]
    Named(ShortDynamicData),
}

/// X_0.
/// See https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#x-0.
#[cfg_attr(feature = "fuzzing", derive(fuzzcheck::DefaultMutator))]
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct X0 {
    pub entrypoint: Entrypoint,
    pub value: tezos_michelson::micheline::Micheline,
}

impl NomReader for X0 {
    fn nom_read(input: &[u8]) -> tezos_encoding::nom::NomResult<Self> {
        let (remaining_input, entrypoint) =
            Entrypoint::nom_read(input).expect("decoded entrypoint");
        let mich_size = u32::from_be_bytes(remaining_input[0..4].try_into().unwrap()) as usize;
        let value =
            tezos_michelson::micheline::Micheline::from_bytes(&remaining_input[4..(mich_size + 4)])
                .expect("decoded value");
        let x0 = X0 { entrypoint, value };
        tezos_encoding::nom::NomResult::Ok((&remaining_input[mich_size + 4..], x0))
    }
}

impl HasEncoding for X0 {
    fn encoding() -> tezos_encoding::encoding::Encoding {
        tezos_encoding::encoding::Encoding::Custom
    }
}

impl BinWriter for X0 {
    fn bin_write(&self, output: &mut Vec<u8>) -> tezos_encoding::enc::BinResult {
        let mut entrypoint_bin: Vec<u8> = Vec::new();
        self.entrypoint.bin_write(&mut entrypoint_bin)?;
        // there is a parameter field:
        tezos_encoding::enc::put_bytes(&entrypoint_bin, output);

        match self.value.to_bytes() {
            Err(e) => Err(tezos_encoding::enc::BinError::custom(format!(
                "could not deserialize value : {:?}",
                e
            ))),
            Ok(value_bin) => {
                let len: u32 = value_bin.len() as u32;
                tezos_encoding::enc::put_bytes(&len.to_be_bytes(), output);
                tezos_encoding::enc::put_bytes(&value_bin, output);
                Ok(())
            }
        }
    }
}
/// Transaction (tag 108).
/// See [https://tezos.gitlab.io/shell/p2p_api.html?highlight=p2p%20encodings#transaction-tag-108].
#[cfg_attr(feature = "fuzzing", derive(fuzzcheck::DefaultMutator))]
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize, HasEncoding, NomReader, BinWriter)]
pub struct TransactionOperation {
    pub source: SignaturePublicKeyHash,
    pub fee: Mutez,
    pub counter: Mutez,
    pub gas_limit: Mutez,
    pub storage_limit: Mutez,
    pub amount: Mutez,
    pub destination: ContractId,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub parameters: Option<X0>,
}

#[cfg_attr(feature = "fuzzing", derive(fuzzcheck::DefaultMutator))]
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize, HasEncoding, BinWriter, NomReader)]
#[encoding(tags = "u8")]
#[serde(tag = "kind", rename_all = "snake_case")]
pub enum Contents {
    /// Seed_nonce_revelation (tag 1).
    #[encoding(tag = 1)]
    SeedNonceRevelation(SeedNonceRevelationOperation),

    /// Double_endorsement_evidence (tag 2).
    DoubleEndorsementEvidence(DoubleEndorsementEvidenceOperation),

    /// Double_baking_evidence (tag 3).
    DoubleBakingEvidence(DoubleBakingEvidenceOperation),

    /// Activate_account (tag 4).
    ActivateAccount(ActivateAccountOperation),

    /// Proposals (tag 5).
    Proposals(ProposalsOperation),

    /// Ballot (tag 6).
    Ballot(BallotOperation),

    /// Double_preendorsement_evidence (tag 7)
    DoublePreendorsementEvidence(DoublePreendorsementEvidenceOperation),

    /// Failing_noop (tag 17).
    #[encoding(tag = 17)]
    FailingNoop(FailingNoopOperation),

    /// Preendorsement (tag 20)
    #[encoding(tag = 20)]
    Preendorsement(PreendorsementOperation),

    /// Endorsement (tag 21)
    #[encoding(tag = 21)]
    Endorsement(EndorsementOperation),

    /// Reveal (tag 107).
    #[encoding(tag = 107)]
    Reveal(RevealOperation),

    /// Transaction (tag 108).
    #[encoding(tag = 108)]
    Transaction(TransactionOperation),

    /// Origination (tag 109).
    #[encoding(tag = 109)]
    Origination(OriginationOperation),

    /// Delegation (tag 110).
    #[encoding(tag = 110)]
    Delegation(DelegationOperation),

    /// Register_global_constant (tag 111).
    #[encoding(tag = 111)]
    RegisterGlobalConstant(RegisterGlobalConstantOperation),

    /// Set_deposits_limit (tag 112).
    #[encoding(tag = 112)]
    SetDepositsLimit(SetDepositsLimitOperation),
}

impl BinWriter for Entrypoint {
    fn bin_write(&self, output: &mut Vec<u8>) -> tezos_encoding::enc::BinResult {
        match self {
            Entrypoint::Default => {
                println!("default");
                put_bytes(&0u8.to_be_bytes(), output)
            }
            Entrypoint::Root => put_bytes(&1u8.to_be_bytes(), output),
            Entrypoint::Do => put_bytes(&2u8.to_be_bytes(), output),
            Entrypoint::SetDelegate => put_bytes(&3u8.to_be_bytes(), output),
            Entrypoint::RemoveDelegate => put_bytes(&4u8.to_be_bytes(), output),
            Entrypoint::Deposit => put_bytes(&5u8.to_be_bytes(), output),
            Entrypoint::Stake => put_bytes(&6u8.to_be_bytes(), output),
            Entrypoint::Unstake => put_bytes(&7u8.to_be_bytes(), output),
            Entrypoint::FinalizeUnstake => put_bytes(&8u8.to_be_bytes(), output),
            Entrypoint::SetDelegateParameters => put_bytes(&9u8.to_be_bytes(), output),
            Entrypoint::Named(a) => {
                put_bytes(&255u8.to_be_bytes(), output);
                let len: u8 = a.data.len() as u8;
                put_bytes(&len.to_be_bytes(), output);
                put_bytes(&a.data.clone(), output)
            }
        }

        Ok(())
    }
}

impl Entrypoint {
    fn to_string_representation(&self) -> String {
        match self {
            Entrypoint::Default => "default".to_string(),
            Entrypoint::Root => "root".to_string(),
            Entrypoint::Do => "do".to_string(),
            Entrypoint::SetDelegate => "set_delegate".to_string(),
            Entrypoint::RemoveDelegate => "remove_delegate".to_string(),
            Entrypoint::Deposit => "deposit".to_string(),
            Entrypoint::Stake => "stake".to_string(),
            Entrypoint::Unstake => "unstake".to_string(),
            Entrypoint::FinalizeUnstake => "finalize_unstake".to_string(),
            Entrypoint::SetDelegateParameters => "set_delegate_parameters".to_string(),

            Entrypoint::Named(a) => {
                let res = a.data.to_base58check();
                match res {
                    Ok(res) => res,
                    Err(e) => {
                        println!("Error reading named entrypoint : {:?}", e);
                        "named".to_string()
                    }
                }
            }
        }
    }
}

impl serde::Serialize for Entrypoint {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string_representation())
    }
}

impl<'de> serde::Deserialize<'de> for Entrypoint {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct EntrypointVisitor;

        impl<'de> serde::de::Visitor<'de> for EntrypointVisitor {
            type Value = Entrypoint;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                formatter.write_str("entrypoint")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                match v {
                    "default" => Ok(Entrypoint::Default),
                    "do" => Ok(Entrypoint::Do),
                    "root" => Ok(Entrypoint::Root),
                    "set_delegate" => Ok(Entrypoint::SetDelegate),

                    "remove_delegate" => Ok(Entrypoint::RemoveDelegate),

                    "deposit" => Ok(Entrypoint::Deposit),
                    "stake" => Ok(Entrypoint::Stake),
                    "unstake" => Ok(Entrypoint::Unstake),
                    "finalize_unstake" => Ok(Entrypoint::FinalizeUnstake),
                    "set_delegate_parameters" => Ok(Entrypoint::SetDelegateParameters),

                    a => Ok(Entrypoint::Named(ShortDynamicData {
                        data: a.as_bytes().to_vec(),
                    })),
                }
            }
        }

        deserializer.deserialize_string(EntrypointVisitor)
    }
}

/// Operation contents.
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize, HasEncoding, NomReader, BinWriter)]
pub struct OperationContents {
    #[encoding(reserve = "Signature::hash_size()")]
    pub contents: Vec<Contents>,
    pub signature: Signature,
}

impl FromShell<P2POperation> for OperationContents {
    type Error = BinaryReaderError;

    fn convert_from(operation: &P2POperation) -> Result<Self, Self::Error> {
        use crate::p2p::binary_message::BinaryRead;
        let OperationContents {
            contents,
            signature,
        } = OperationContents::from_bytes(operation.data())?;
        Ok(OperationContents {
            contents,
            signature,
        })
    }
}

/**
 * Operation contents.
+-----------+----------+----------------------------------------------------+
| Name      | Size     | Contents                                           |
+===========+==========+====================================================+
| branch    | 32 bytes | bytes                                              |
+-----------+----------+----------------------------------------------------+
| contents  | Variable | sequence of $012-Psithaca.operation.alpha.contents |
+-----------+----------+----------------------------------------------------+
| signature | 64 bytes | bytes                                              |
+-----------+----------+----------------------------------------------------+
 */
#[cfg_attr(feature = "fuzzing", derive(fuzzcheck::DefaultMutator))]
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize, HasEncoding, NomReader, BinWriter)]
pub struct Operation {
    pub branch: BlockHash,
    #[encoding(reserve = "Signature::hash_size()")]
    pub contents: Vec<Contents>,
    pub signature: Signature,
}

#[derive(Debug, thiserror::Error)]
pub enum OperationVerifyError {
    #[error("invalid operation contents")]
    InvalidContents,
    #[error("cannot encode operation: `{0}`")]
    ToBytes(#[from] BinaryWriterError),
    #[error("cryptography error: `{0}`")]
    Crypto(#[from] CryptoError),
}

impl Operation {
    pub fn verify_signature(
        &self,
        pk: &SignaturePublicKey,
        chain_id: &ChainId,
    ) -> Result<bool, OperationVerifyError> {
        let watermark = match self.contents.split_first() {
            Some((Contents::Preendorsement(_), [])) => SignatureWatermark::Preendorsement(chain_id),
            Some((Contents::Endorsement(_), [])) => SignatureWatermark::Endorsement(chain_id),
            _ => SignatureWatermark::GenericOperation,
        };
        let encoded_contents = self.contents.iter().try_fold(
            Vec::new(),
            |mut acc, item| -> Result<_, BinaryWriterError> {
                acc.extend(item.as_bytes()?);
                Ok(acc)
            },
        )?;
        let bytes: [&[u8]; 2] = [self.branch.as_ref(), encoded_contents.as_slice()];
        let result = pk.verify_signature(&self.signature, watermark, bytes)?;
        Ok(result)
    }
}

impl FromShell<P2POperation> for Operation {
    type Error = BinaryReaderError;

    fn convert_from(operation: &P2POperation) -> Result<Self, Self::Error> {
        use crate::p2p::binary_message::BinaryRead;
        let branch = operation.branch().clone();
        let OperationContents {
            contents,
            signature,
        } = OperationContents::from_bytes(operation.data())?;
        Ok(Operation {
            branch,
            contents,
            signature,
        })
    }
}

impl Operation {
    pub fn is_endorsement(&self) -> bool {
        self.as_endorsement().is_some()
    }

    pub fn as_endorsement(&self) -> Option<&EndorsementOperation> {
        if let Some((Contents::Endorsement(endorsement), [])) = self.contents.split_first() {
            Some(endorsement)
        } else {
            None
        }
    }

    pub fn is_preendorsement(&self) -> bool {
        self.as_preendorsement().is_some()
    }

    pub fn as_preendorsement(&self) -> Option<&PreendorsementOperation> {
        if let Some((Contents::Preendorsement(preendorsement), [])) = self.contents.split_first() {
            Some(preendorsement)
        } else {
            None
        }
    }

    pub fn payload(&self) -> Option<&BlockPayloadHash> {
        if let Some((contents, [])) = self.contents.split_first() {
            match contents {
                Contents::Endorsement(EndorsementOperation {
                    block_payload_hash, ..
                })
                | Contents::Preendorsement(PreendorsementOperation {
                    block_payload_hash, ..
                }) => Some(block_payload_hash),
                _ => None,
            }
        } else {
            None
        }
    }

    pub fn level_round(&self) -> Option<(i32, i32)> {
        if let Some((contents, [])) = self.contents.split_first() {
            match contents {
                Contents::Endorsement(EndorsementOperation { level, round, .. })
                | Contents::Preendorsement(PreendorsementOperation { level, round, .. }) => {
                    Some((*level, *round))
                }
                _ => None,
            }
        } else {
            None
        }
    }

    pub fn slot(&self) -> Option<u16> {
        if let Some((contents, [])) = self.contents.split_first() {
            match contents {
                Contents::Endorsement(EndorsementOperation { slot, .. })
                | Contents::Preendorsement(PreendorsementOperation { slot, .. }) => Some(*slot),
                _ => None,
            }
        } else {
            None
        }
    }
}
