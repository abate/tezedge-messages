// Copyright (c) SimpleStaking, Viable Systems and Tezedge Contributors
// SPDX-License-Identifier: MIT

pub mod constants;
pub mod contract;
pub mod operation;
pub mod rights;
pub mod votes;

pub const PROTOCOL_HASH: &str = "PtLimaPtLMwfNinJi9rCfDPWea8dFgTZ1MeJ9f1m2SRic6ayiwW";
