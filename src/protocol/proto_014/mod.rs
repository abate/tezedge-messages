// Copyright (c) SimpleStaking, Viable Systems and Tezedge Contributors
// SPDX-License-Identifier: MIT

pub mod constants;
pub mod contract;
pub mod operation;
pub mod rights;
pub mod votes;

pub const PROTOCOL_HASH: &str = "PtKathmankSpLLDALzWw7CGD2j2MtyveTwboEYokqUCP4a1LxMg";
