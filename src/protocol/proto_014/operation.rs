// Copyright (c) SimpleStaking, Viable Systems and Tezedge Contributors
// SPDX-License-Identifier: MIT

//! Operation contents. This is the contents of the opaque field [super::operation::Operation::data].
//!
//! Changes comparing to the [`proto_013::operation::Operation`](crate::proto_013::operation::Operation):
//!
//! TODO
pub use super::super::proto_013::operation::*;
